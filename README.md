<p align="center">
  <img src="https://github.com/CulurjonisCoin/culurjonis/blob/master/logo.png" width="50%" height="50%"/></p>


# What is Culurjonis?
Culurjonis is designed to be a feeless, instant, high throughput cryptocurrency for the meme economy. 

### Key features
* Culurjonis utilizes a novel block-lettuce architecture.
* Instant and Feeless transactions.
* Fungible (& Edible).
* Daily carbohydrate airdrops.
* Ripe for memes.
* Literally grows on earth, no mining required.
* Not a Shitcoin (until digested).
* No more confusing prefix, Culurjonis contain Culurjonishi.
* Replay Protection to stop attackers from executing replay attacks from chain X on chain Y and vice versa.

### Key facts
* Forked from BANANO
* Total Supply: 3,402,823,669.20
* Born in 10 Jul. 2020, Mainnet launch April 1st, 2021

For more information, see [Culurjonis.cc](https://culurjonis.cc).

### Resources
- [Culurjonis website](https://culurjonis.cc)
- [Yellowpaper](https://culurjonis.cc/)
- [Discord chat](https://chat.culurjonis.cc)
- [Reddit](http://reddit.com/r/culurjonis)
- [Fritter](http://twitter.com/culurjoniscoin)
- [GitHub wiki](http://github.com/culurjoniscoin/culurjonis/wiki)
- [CulurjonisVault](https://vault.culurjonis.cc)
- [Telegram](https://t.me/joinchat/CN6PzBsZTeL7z3JI_dmXFg)
- [Culurjonis.how all links at a glance](http://culurjonis.how/)

### Build instructions
- [Building Culurjonis from source](https://github.com/CulurjonisCoin/culurjonis/wiki/Building-a-Culurjonisde-from-sources)

### Running a Docker node
- [Running a Docker node](https://github.com/CulurjonisCoin/culurjonis/wiki/Running-a-Docker-Culurjonisde)

### Want to contribute?

### Contact us
You can reach us via the [Discord](https://chat.culurjonis.cc) or our [Reddit](http://reddit.com/r/culurjonis).
You can also [file an issue](http://github.com/culurjoniscoin/culurjonis/issues).